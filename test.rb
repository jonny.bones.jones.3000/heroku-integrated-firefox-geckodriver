require 'selenium-webdriver'

def run_selenium
  options = Selenium::WebDriver::Firefox::Options.new
  options.add_argument('--remote-debugging-port=9222')
  options.add_argument('--headless')
  options.add_argument('--disable-gpu')
  options.add_argument('--no-sandbox')

  Selenium::WebDriver::Firefox::Binary.path = ENV['FIREFOX_BIN']
  Selenium::WebDriver::Firefox::Service.driver_path = ENV['GECKODRIVER_PATH']

  # use argument `:debug` instead of `:info` for detailed logs in case of an error
  Selenium::WebDriver.logger.level = :info

  driver = Selenium::WebDriver.for :firefox, options: options
  driver.get "https://www.google.com"
  puts "#{driver.title}"
  driver.quit
end

# Call the function
run_selenium
